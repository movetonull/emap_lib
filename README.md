EmAP.

EmAP (Embedded Audio Player) - проект для воспроизведения аудио на embedded платформах (например, микроконтроллеры stm32, avr), с возможностью изменения громкости, скорости, темпа воспроизведения.

Состав.

В проекте существует разделение на уровни и независимые подмодули.
Самое наглядное разделение: Player -> Decoder -> Stream
В проекте используется следующие библиотеки:

•	The Helix MP3 Decoder – декодер mp3.

•	Sonic library – библиотека обработки аудио потока.

Характеристики.

Поддерживаемые форматы: 

•	mp3

•	wav

Интегрирована библиотека Sonic, которая позволяет управлять следующими параметрами:

•	громкость

•	скорость 

•	тембр 

Ссылки.

Библиотека sonic.
https://github.com/waywardgeek/sonic

MP3 декодер 
https://datatype.helixcommunity.org/Mp3dec

https://bitbucket.org/movetonull/emap_lib/src/360e14844a4cdec4e6d9b5e852ababea3909cfa5/Documentation/doxygen/html/index.html?at=develop&fileviewer=file-view-default